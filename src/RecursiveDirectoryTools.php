<?php
/**
 * File system related library.
 *
 * Recursive directory tools.
 *
 * @copyright  <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license    https://opensource.org/licenses/mit-license.php
 * @version    {$Id}
 */

namespace donbidon\Lib\FileSystem;

use InvalidArgumentException;
use RuntimeException;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;

/**
 * File system related library.
 *
 * Recursive directory tools.
 *
 * @static
 */
class RecursiveDirectoryTools
{
    /**
     * Walks directory recursively.
     *
     * ```php
     * use donbidon\Lib\FileSystem\RecursiveDirectoryTools;

     * function walker(\SplFileInfo $file)
     * {
     *     $path = $file->getRealPath();
     *     echo sprintf(
     *         "[%s] %s%s", $file->isDir() ? "D" : "F",
     *         $path,
     *         PHP_EOL
     *     );
     * }
     *
     * RecursiveDirectoryTools::walk("/path/to/dir", 'rmFile');
     * ```
     *
     * @param  string   $path
     * @param  callback $callback
     * @return void
     * @throws InvalidArgumentException  If passed path isn't a directory.
     * @throws RuntimeException  If Passed path cannot be read.
     */
    public static function walk($path, $callback)
    {
        $realPath = realpath($path);
        if (!is_dir($realPath)) {
            throw new InvalidArgumentException(sprintf(
                "Passed path '%s' isn't a directory", $realPath
            ));
        }
        if (!is_readable($realPath)) {
            throw new \RuntimeException(sprintf(
                "Passed path '%s' cannot be read", $realPath
            ));
        }
        $dir = new RecursiveDirectoryIterator(
            $realPath,
            RecursiveDirectoryIterator::SKIP_DOTS
        );
        $files = iterator_to_array(new RecursiveIteratorIterator(
                $dir,
                RecursiveIteratorIterator::CHILD_FIRST
            )
        );
        array_walk($files, $callback, ['path' => $path]);
    }

    /**
     * Removes directory recursively.
     *
     * @param  string $path
     * @param  bool   $clearStatCache
     * @return void
     */
    public static function remove($path, $clearStatCache = TRUE)
    {
        self::walk($path, [__CLASS__, 'rmFile']);
        rmdir($path);
        if ($clearStatCache) {
            clearstatcache(NULL, $path);
        }
    }

    /**
     * Callback using for removing directory.
     *
     * Removes file or directory.
     *
     * @param  SplFileInfo $file
     * @return void
     * @see    self::remove()
     * @internal
     */
    protected static function rmFile(SplFileInfo $file)
    {
        $path = $file->getRealPath();
        $file->isDir() ? rmdir($path) : unlink($path);
    }
}
