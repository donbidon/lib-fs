# File system related library

## Installing
Run ` composer --dev require donbidon/lib-fs dev-master ` or add following code to your "composer.json" file:
```json
    "repositories": [
        {
            "type": "vcs",
            "url":  "https://bitbucket.org/donbidon/lib-fs"
        }
    ],
    "require": {
        "donbidon/lib-fs": "dev-master"
    }
```
and run `composer update`.

## So what's inside?

### Walking directory recursively
```php
function walker(\SplFileInfo $file)
{
    $path = $file->getRealPath();
    echo sprintf(
        "[%s] %s%s", $file->isDir() ? "D" : "F",
        $path,
        PHP_EOL
    );
}
\donbidon\Lib\FileSystem\RecursiveDirectoryTools::walk("/path/to/dir", 'walker');

### Removing directory recursively
```php
\donbidon\Lib\FileSystem\RecursiveDirectoryTools::remove("/path/to/dir");
```

### Logging functionality supporting files rotation
```php
$logger = new \donbidon\Lib\FileSystem\Logger([
    'path'    => "/path/to/log",
    // 'maxSize' => int maxSize,   // Logger::DEFAULT_MAX_SIZE by default
    // 'rotation' => int rotation, // rotated files number
    // 'righs'    => int rights,   // if set after writing to log file chmod() will be called
]);
$logger->log("Foo");
```
